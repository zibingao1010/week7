# Week 7 Mini-Project

## Instructions
1. Create a new Rust project by running `cargo new <PROJECT_NAME>`
2. Add necessary dependencies to the Cargo.toml by running
    ```
    cargo add qdrant-client anyhow tonic tokio serde-json --features tokio/rt-multi-thread
    ```
3. Launch the Qdrant database with enabled gRPC interface by running
    ```
    docker run -p 6333:6333 -p 6334:6334 \
        -e QDRANT__SERVICE__GRPC_PORT="6334" \
        qdrant/qdrant
    ```
4. Add implementations in `main.rs`.
5. Run `cargo run` to test output.

## Sreenshots
### Connect to Qdrant vector database
![image](Qdrant.png)

### Ingest data into Vector database
1. **Loop Execution**: The code runs a loop ten times (from `0` to `9`). This is an increase from the original version where the loop ran five times. The increased count allows for more data points to be ingested into the collection.
2. **Data Structure Creation**: Inside the loop, for each iteration, a JSON payload is created with structured data. This data varies in each iteration to ensure diversity among the ingested data points. The payload structure has been modified compared to the original, now including keys `category`, `value`, and `details`, with `details` containing nested information.
3. **Vector Creation**: Alongside the payload, a vector is generated for each point. The vector consists of twelve floating-point numbers (an increase from the original ten), each initialized to `10.0 + i` (where `i` is the current loop index). This change slightly alters the vector values for each data point.
4. **Data Insertion**: Each loop iteration constructs a `PointStruct` with the current index, the generated vector, and the payload. These points are then inserted (upserted) into the collection specified by `collection_name` using the `upsert_points_blocking` method. Upsert here means that the point is inserted if new, or updated if it already exists based on the point's id.
5. **Error Handling**: Each payload conversion and data insertion is wrapped in error handling to catch and report any issues during the data ingestion process.

### Perform queries and aggregations
1. **Query Setup**: A query is constructed to search for points in the specified collection (`sample_collection`). The query seeks points that are similar to a predefined vector, here set to a 12-dimensional vector filled with `15.0`. This represents a change from the original query vector to reflect the updated vector sizes.
2. **Filter Application**: Unlike the original code, a filter condition is applied to narrow down the search results. The filter specifies that only points where the `value` field ranges between `25` and `30` should be returned. This is an additional constraint to retrieve more specific results from the database.
3. **Query Execution**: The search query, along with its configurations (such as the absence or presence of filters, the number of results to return, and whether to include payload data), is executed against the collection. In this case, the code requests up to ten matching points – an increase from the original limit of five.
4. **Results Handling**: The search results are iterated over, and for each found point, its index (starting from one) and payload are printed out. This part of the code visualizes the query results, allowing the user to see which points were returned by the query and what data they contain.

### Visualize output
![image](Output.png)

