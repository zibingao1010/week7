use anyhow::{Result, anyhow};
use qdrant_client::prelude::*;
use qdrant_client::qdrant::{
    Condition, CreateCollection, Filter, SearchPoints, VectorParams, VectorsConfig,
};
use qdrant_client::qdrant::vectors_config::Config;
use serde_json::json;

#[tokio::main]
async fn main() -> Result<()> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;

    let collection_name = "sample_collection";
    let _ = client.delete_collection(collection_name).await;

    client.create_collection(&CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 12, 
                distance: Distance::Euclid.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    for i in 0..10 { 
        let payload: Payload = json!({
            "category": format!("Type{}", i),
            "value": 20 + i, 
            "details": {"sub": format!("Detail{}", i)} 
        }).try_into().map_err(|e| anyhow!("Payload conversion error: {:?}", e))?;
        
        let points = vec![PointStruct::new(i, vec![10.0 + i as f32; 12], payload)]; 
        client.upsert_points_blocking(collection_name, None, points, None).await?;
    }

    let search_result = client.search_points(&SearchPoints {
        collection_name: collection_name.into(),
        vector: vec![15.; 12], 
        filter: None,
        limit: 10, 
        with_payload: Some(true.into()), 
        ..Default::default()
    }).await?;

    for (index, point) in search_result.result.iter().enumerate() {
        println!("Point {} Payload: {:?}", index + 1, point.payload);
    }

    Ok(())
}
